\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{my-cls}[2021/03/06 my-cls v0.1]

\LoadClass[12pt, a4paper, numbers=endperiod]{scrreport}

% === === === === === === === === === === % 
% === Package === % 
% UTF-8 Codierung
\RequirePackage[utf8]{inputenc}

% Westeuropäischer Codierung
\RequirePackage[T1]{fontenc}

% Spracheinstellungen "ngerman" = Deutsch
\RequirePackage[ngerman]{babel}

% Zitieren Bibtex
\RequirePackage[fixlanguage]{babelbib}
%\RequirePackage{csquotes}

% Schriftartenpacket "Latin Modern"
\RequirePackage{lmodern}

% Abstand bei Aufzählungen minimieren
\RequirePackage{enumitem}

% Grafiken und Bilder einbinden
\RequirePackage{graphicx}

% Einfügung mehrerer Bilder in einer Gleitumgebung.
\RequirePackage{subcaption}

% Gestaltungsmöglichkeiten bezüglich der Beschriftung von Bildern 
% Bildunterschrift kleiner machen
\RequirePackage[labelfont={bf,sf},font={footnotesize},labelsep=space]{caption}

% Zeilenabstand ändern: single-, onehalf-, double-, begin{...-space}, \...-spacing
\RequirePackage{setspace}

% Einrücken der ersten Zeile nach Section
\RequirePackage{indentfirst}

% Styling Kopf- und Fusszeile
\RequirePackage[automark,headsepline]{scrlayer-scrpage}

% Farben
\RequirePackage[table]{xcolor}

% Quellcode einbinden
\RequirePackage{listings}

% Transparenz 
\RequirePackage{transparent}

% (SI-)Einheiten darstellen
\RequirePackage{siunitx}

% Verbundene Zeilen und Spalten
\RequirePackage{multicol}
\RequirePackage{multirow}

% Landscape ermöglichen
\RequirePackage{pdflscape} 

% Blindtext \blindtext
\RequirePackage{blindtext} % \blindtext
\RequirePackage{lipsum} % \lipsum[1-5]

% zusätzliche Schriftzeichen der American Mathematical Society 
\RequirePackage{amssymb}
\RequirePackage{amsfonts}
\RequirePackage{amsmath}
\RequirePackage{empheq}

% Für lstlistoflistings
\RequirePackage{scrhack}

% Float Container
\RequirePackage{float}


% TODO: Benötigt?
% = Kein Plan =
% Ihrgend was mit Tabellen
% \usepackage{array}
% \newcolumntype{P}[1]{>{\raggedright\arraybackslash}p{#1}}
% 
%\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
% \newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
% \newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% = Kein Plan =
% \usepackage{courier}


% TODO: Zitieren nicht gebraucht?
% === === === === === === === === === === % 
% === Zitieren === % 
% https://www.overleaf.com/learn/latex/Articles/Getting_started_with_BibLaTeX 
% \usepackage[backend=bibtex,style=authortitle]{biblatex}
% \addbibresource{quellen.bib}


% === === === === === === === === === === % 
% === Änderungen === % 
% Schriftart sans serif
\renewcommand{\familydefault}{\sfdefault}

% Titel "Chapter" Abstand
\renewcommand*\chapterheadstartvskip{\vspace*{-\topskip}}
\renewcommand*\chapterheadendvskip{	\vspace*{0pt}}

% Einstellung des Grafikpfads
\graphicspath{{img/}}

% Abstand nach Absatz
\setlength{\parskip}{0.5em}

% Abstand bei Aufzählungen minimieren
\setlist[enumerate]{itemsep=1pt,parsep=0pt}
\setlist[itemize]{itemsep=1pt,parsep=0pt}

% Geänderte Symbole Aufzählung
\renewcommand{\labelitemi}{$\bullet$}
\renewcommand{\labelitemii}{$\circ$}
\renewcommand{\labelitemiii}{--}
\renewcommand{\labelitemiv}{-} 

% zusätzliche Ebenen in der Gliederung, Nummerierung, Inhaltsverzeichnis
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

% Zitiersprache
\selectbiblanguage{german}

% Subparagraph einrückung
%\RedeclareSectionCommand[indent=0pt]{subparagraph}


% === === === === === === === === === === % 
% === Kopf- und Fußzeile === % 
% Kopfzeile
\lohead{DRL}
\chead{}
\rohead{\headmark}
% Fusszeile
\lofoot{}
%\lofoot{OST, FHGR}
\cfoot{\pagemark}
\rofoot{}
%\rofoot{R. Good, E. Meyer}

% === === === === === === === === === === % 
% === Speziel === % 
% Besondere Trennungen
\hyphenation{De-zi-mal-tren-nung}

% === === === === === === === === === === % 
% === Quellcode === % 
% Farben
\definecolor{scgray}{rgb}{0.5,0.5,0.5}
\definecolor{scgreen}{rgb}{0,0.6,0}
\definecolor{scorange}{rgb}{1,0.5,0}
\definecolor{scpurple}{rgb}{0.6,0,0.8}

% Styling
\lstset{
  backgroundcolor=\color{white},     
  basicstyle=\footnotesize,  
  breakatwhitespace=false,
  breaklines=true,
  captionpos=b,
  commentstyle=\color{scgreen},
  frame=single,
  keepspaces=true, 
  keywordstyle=\color{scpurple},
  numbers=left,
  numbersep=10pt,                   % numbersep=5pt,
  numberstyle=\tiny\color{scgray}, 
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,
  showstringspaces=false,          
  showtabs=false,
  stepnumber=1,
  stringstyle=\color{scorange},
  tabsize=2,
  title=\lstname          
}

% TODO: Erweiterbar
\lstdefinestyle{JS}{
  morekeywords={
    document,
    getElementById,
    addEventListener,
    classList,
    getBoundingClientRect,
    var,
    let,
    const
  }
}

% TODO: Erweiterbar
\lstdefinestyle{CSS}{
  morekeywords={
    @import,
    url,
    html,
    body,
    head,
    h1,h2,h3,h4,h5,h6,
    p,
    ul,ol,
    a,
    nth-of-type,
    nth-child,
  }
}

% Listing auf Code und Codesverzeichnis
\renewcommand{\lstlistingname}{Code}
\renewcommand{\lstlistlistingname}{\lstlistingname sverzeichnis}