
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

class Cliff:

    def __init__(self, rows=4, cols=12):
        self.rows = rows
        self.cols = cols
        self.start = (rows - 1, 0)
        self.goal = (rows - 1, cols - 1)
        self.end = False
        self.position = self.start
        self.board = np.zeros([rows, cols])
        self.board[rows - 1, 1:(cols - 1)] = -1
    
    def step(self, action):
        if action == 0: # up
            # next_position = (self.position[0], self.position[1] + 1)
            next_position = (self.position[0] - 1, self.position[1])
        elif action == 1: # down
            next_position = (self.position[0] + 1, self.position[1])
        elif action == 2: # left
            next_position = (self.position[0], self.position[1] - 1)
        else: # right
            next_position = (self.position[0], self.position[1] + 1)
            # next_position = (self.position[0] - 1, self.position[1])
        
        if next_position[0] >= 0 and next_position[0] <= self.rows - 1 and next_position[1] >= 0 and next_position[1] <= self.cols - 1:
                self.position = next_position

        if self.position == self.goal:
            self.end = True
            print("yeeee 💃🎯🏆")
        if self.board[self.position] == -1:
            self.end = True
            print("ohhw noou abaghait💀👻")
        
        return self.position, self.__reward(), self.end

    def reset(self):
        self.end = False
        self.position = self.start

    def __reward(self):
        if self.board[self.position] == -1:
            return -100
        if self.position == self.goal:
            return 0;
        return -1


class Agent:
    def __init__(self, epsilon=0.2, alpha=0.1, gamma=0.95):
        self.cliff = Cliff()
        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma
        self.position = self.cliff.start
        self.actions = [0, 1, 2, 3]
        self.total_reward = 0
        self.Q = {}
        self.e = {}
        self.delta = 0
        self.lamb = 1
        self.rewards = []
        for i in range(self.cliff.rows):
            for j in range(self.cliff.cols):
                self.Q[(i, j)] = {}
                self.e[(i, j)] = {}
                for a in self.actions:
                    self.Q[(i, j)][a] = -np.random.random()
                    self.e[(i, j)][a] = 0

    def choose_action(self, state):
        action = 0
        if np.random.uniform(0, 1) < self.epsilon:
            action = np.random.choice(self.actions)
        else:
            action = np.argmax(list(self.Q[state].values()))
        return action

    def update_e(self, state, action, reward, state2, action2):
        self.delta = reward + self.gamma * self.Q[state2][action2] - self.Q[state][action]
        self.e[state][action] = self.e[state][action] + 1

    def update_all(self, state, action):
        self.Q[state][action] = self.Q[state][action] + self.alpha * self.delta * self.e[state][action]
        self.e[state][action] = self.gamma * self.lamb * self.e[state][action]

    def reset(self):
        self.cliff.reset()
        self.position = self.cliff.start

    def learn(self, n_episodes):
        for episode in range(n_episodes):
            self.reset()
            reward = 0
            state = self.position
            action = 0
            states = []

            while (True):
                # get next state
                state2, r, end = self.cliff.step(action)

                # decrease espilon and lambda
                self.epsilon *= 0.9999
                self.lamb *= 0.9999

                # choose next action
                action2 = self.choose_action(state2)

                # learn Q value
                self.update_e(state, action, r, state2, action2)

                # update all state-action pairs
                for state in self.Q:
                    for action in self.Q[state]:
                        self.update_all(state, action)
                        
                # set new state & action as current
                state = state2
                action = action2

                # append state for route monitoring
                states.append(state)

                # update reward
                reward += r

                # leave loop if state is terminal
                if end:
                    break

            self.total_reward += reward
            self.rewards.append(reward)
            print("episode {} finished with reward {}".format(episode, reward))
            # print(states)
            # print(self.epsilon)
        print("total reward: {}".format(self.total_reward))
        print("mean: {}".format(self.total_reward / n_episodes))



agent_lambda = Agent()
agent_lambda.learn(5000)
