import sys

sys.path.append("/home/eldr0n/anaconda3/envs/rl2/lib/python3.6/site-packages")

import numpy as np
import gym
import matplotlib
import matplotlib.pyplot as plt
import random

env = gym.make("CartPole-v1")
# Observation:
#         Type: Box(4)
#         Num     Observation               Min                     Max
#         0       Cart Position             -4.8                    4.8
#         1       Cart Velocity             -Inf                    Inf
#         2       Pole Angle                -0.418 rad (-24 deg)    0.418 rad (24 deg)
#         3       Pole Angular Velocity     -Inf                    Inf
#     Actions:
#         Type: Discrete(2)
#         Num   Action
#         0     Push cart to the left
#         1     Push cart to the right
#         Note: The amount the velocity that is reduced or increased is not
#         fixed; it depends on the angle the pole is pointing. This is because
#         the center of gravity of the pole increases the amount of energy needed
#         to move the cart underneath it
#     Reward:
#         Reward is 1 for every step taken, including the termination step


alpha = 0.1
gamma = 0.95
epsilon = 1
epsilon_min = 0.05
epsilon_decay = 0.99995

Observation = [30, 30, 50, 50]
np_array_win_size = np.array([0.25, 0.25, 0.01, 0.1])

Q_values = np.random.uniform(low=0,
                             high=1,
                             size=(Observation + [env.action_space.n]))
max_episodes = 600000

total_reward = 0


def get_discrete_state(state):
    discrete_state = state / np_array_win_size + np.array([15, 10, 1, 10])
    return tuple(discrete_state.astype(np.int))


def get_epsilon_greedy_action(state):
    if random.random() <= epsilon:
        return np.random.randint(0, env.action_space.n)  
    else:
        return np.argmax(Q_values[state])

def update_Q(state, state2, action):
    qMax = np.max(Q_values[state2])
    qsa = Q_values[state + (action,)]
    rpe = r + gamma * qMax - qsa
    Q_values[state + (action,)] += rpe * alpha

for episode in range(max_episodes):  # we could do this with a while...
    state = get_discrete_state(env.reset())
    done = False
    reward = 0
    
    while (not done):  # and/or we could do this with a for ...

        action = get_epsilon_greedy_action(state)
        new_state, r, done, _ = env.step(action)
        reward += r
        state2 = get_discrete_state(new_state)

        if episode % 2000 == 0:
            env.render()

        update_Q(state, state2, action)

        state = state2

    epsilon *= epsilon_decay
    if epsilon < epsilon_min:
        epsilon = epsilon_min
        
    print("episode {} finished with reward {}".format(episode, reward))

env.close()

if done:
    print('env solved!')
else:
    print('env not solved')
    